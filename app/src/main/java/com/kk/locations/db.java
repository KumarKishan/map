package com.kk.locations;

/**
 * Created by shubhamraj on 6/4/16.
 */


import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;


public class db extends SQLiteOpenHelper {

    private static final String dbName = "Maps.db";

    public db(Context context)
    {
        super(context,dbName,null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL("Create table locations (id integer primary key, name text, latitude real,longitude real)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase database,int oldVersion,int newVersion)
    {
        database.execSQL("Drop table if exists locations");
    }
    public boolean insertLocation(String name,double latitude,double longitude)
    {
        try {
            SQLiteDatabase database = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("name",name);
            contentValues.put("latitude",latitude);
            contentValues.put("longitude",longitude);
            database.insert("locations",null,contentValues);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }

    }

    public ArrayList<Locations> getAllLocations()
    {
        ArrayList<Locations> temp = new ArrayList<Locations>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("Select  * from locations", null);
        cursor.moveToFirst();
        while (cursor.isAfterLast()==false)
        {
            Locations t = new Locations();
            t.setId(cursor.getInt(cursor.getColumnIndex("id")));
            t.setName(cursor.getString(cursor.getColumnIndex("name")));
            t.setLatitude(cursor.getDouble(cursor.getColumnIndex("latitude")));
            t.setLongitude(cursor.getDouble(cursor.getColumnIndex("longitude")));
            temp.add(t);
            cursor.moveToNext();
        }


        return temp;
    }


}
