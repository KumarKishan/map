package com.kk.locations;

/**
 * Created by Kishan Kumar on 6/4/16.
 */
public class Locations {
    private int id;
    private String name;
    private double latitude;
    private double longitude;

    public void setId(int x)
    {
        this.id = x;
    }
    public int getId()
    {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}
