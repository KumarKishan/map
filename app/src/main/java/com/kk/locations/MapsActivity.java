package com.kk.locations;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GPSTracker gpsTracker;
    private CameraPosition camPos;
    private LatLng latLng;
    private double markerLat;
    private double markerLon;
    private float zoomLevel;
    private Button zoomIn;
    private Button zoomOut;
    private Button newMarker;
    private Button saveMarker;
    private Button loadMarker;
    private Button ok;
    private Button infoButton;
    private LinearLayout lLayout;
    private boolean goToHome;
    private boolean isNewMarker;
    private String lName;
    private db dataBase;
    private ConnectivityManager conMgr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        conMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if ( conMgr.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
                || conMgr.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ) {

            // notify user you e online

        }
        else if ( conMgr.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED
                || conMgr.getNetworkInfo(1).getState() == NetworkInfo.State.DISCONNECTED) {

            Toast.makeText(MapsActivity.this, "No Network Connection. App cant function !", Toast.LENGTH_LONG).show();
        }






        gpsTracker = new GPSTracker(this);

        zoomIn = (Button)findViewById(R.id.zoomIn);
        zoomOut = (Button)findViewById(R.id.zoomOut);
        newMarker = (Button)findViewById(R.id.newMarker);
        saveMarker = (Button)findViewById(R.id.saveMarker);
        loadMarker = (Button)findViewById(R.id.loadMarker);
        ok = (Button)findViewById(R.id.okButton);
        infoButton = (Button)findViewById(R.id.infoButton);
        goToHome = true;
        dataBase = new db(this);
        zoomIn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        zoomLevel += 1;
                        zoomLevel = zoomLevel> mMap.getMaxZoomLevel()? mMap.getMaxZoomLevel():zoomLevel;
                        setCamera();
                    }
                }
        );
        zoomOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                zoomLevel = zoomLevel == mMap.getMinZoomLevel() ? mMap.getMinZoomLevel(): zoomLevel - 1;
                setCamera();
            }
        });
        newMarker.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isNewMarker=true;
                addMarker();
            }
        });
        saveMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lLayout = (LinearLayout) findViewById(R.id.lLayout);
                lLayout.setVisibility(View.VISIBLE);

            }
        });
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lName = ((EditText)findViewById(R.id.lName)).getText().toString();

                if(!lName.isEmpty())
                {

                    lLayout.setVisibility(View.INVISIBLE);
                    dataBase.insertLocation(lName,markerLat,markerLon);
                    Toast.makeText(MapsActivity.this, lName +" " +  markerLat + " " +markerLon, Toast.LENGTH_SHORT).show();
                }
                ((EditText)findViewById(R.id.lName)).setText("");
            }
        });
        loadMarker.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ArrayList<Locations> list = dataBase.getAllLocations();
                if(list.size() == 0)
                    Toast.makeText(MapsActivity.this, "Please save locations first to load them", Toast.LENGTH_LONG).show();
                for(int i=0;i<list.size();i++)
                {
                    addMarker(list.get(i));
                }


            }
        });
        infoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, info.class);
                startActivity(intent);

            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);



    }
    public void setLocation()
    {
        double lat = 0;
        double lon = 0;
        if (goToHome)
        {
            if(gpsTracker.canGetLocation)
            {
                lat = gpsTracker.getLatitude();
                lon = gpsTracker.getLongitude();
            }
            goToHome = false;
        }
        else
        {
            latLng = mMap.getCameraPosition().target;
            return;
        }
        latLng = new LatLng(lat,lon);


    }
    private void setCamera()
    {
        setLocation();
        camPos = new CameraPosition.Builder()
                .target(latLng)
                .zoom(zoomLevel)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(camPos));
    }
    private void addMarker(Locations locations)
    {

        mMap.addMarker(new MarkerOptions().position(new LatLng(locations.getLatitude(),locations.getLongitude())).draggable(false).title(locations.getName()));
    }
    private void addMarker()
    {
        CameraPosition cam = mMap.getCameraPosition();
        latLng = cam.target;


        if(isNewMarker)
        {
            mMap.clear();
            markerLat = latLng.latitude;
            markerLon = latLng.longitude;
        }
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }
            @SuppressWarnings("unchecked")
            @Override
            public void onMarkerDrag(Marker marker) {
                mMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                latLng =marker.getPosition();

                markerLat = latLng.latitude;
                markerLon = latLng.longitude;
            }
        });
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        markerLat = latLng.latitude;
        markerLon = latLng.longitude;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        zoomLevel = mMap.getMaxZoomLevel();
        setCamera();
        addMarker();
    }


}
